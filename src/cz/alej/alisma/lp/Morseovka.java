package cz.alej.alisma.lp;

/*
 * Tento program ma prepisovat text do Morseovy abecedy.
 *
 * Zatim ale umi jen nacitat jednotlive znaky na vstupu a
 * preskakovat tzv. bile znaky (mezery, znaky noveho radku
 * apod.).
 *
 * Doplnte implementaci metody prevedZnak, aby umela prevest
 * alespon vsechna pismena. Vyuzijte vhodnou tridu z Java
 * Collections pro ulozeni vlastniho mapovani mezi znakem
 * a jeho morseovou reprezentaci.
 *
 * Pri vstupu "Ahoj svete" ocekavame vystup
 *
 * .-/..../---/.---/.../...-/./-/./
 */

import java.util.Scanner;

public class Morseovka {
    private static final String ODDELOVAC = ".|\\n";
    private static final String NEZNAME = "??";

    private static String prevedZnak(Character znak) {
        if (Character.isWhitespace(znak)) {
            return null;
        }
        return NEZNAME;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        sc.useDelimiter("");
        while (sc.hasNext(ODDELOVAC)) {
            char znak = sc.next(ODDELOVAC).charAt(0);
            String morse = prevedZnak(znak);
            if (morse == null) {
                continue;
            }
            System.out.printf("%s/", morse);
        }
        sc.close();
        System.out.println();
    }

}
